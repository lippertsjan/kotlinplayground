package de.ironjan.kotlinplayground.model

data class ShowInformation(
    val moderator: String,
    val show: String
)