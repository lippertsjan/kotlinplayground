package de.ironjan.kotlinplayground.model

data class Track(
    val artist: String,
    val title: String
)