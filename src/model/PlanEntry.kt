package de.ironjan.kotlinplayground.model

data class PlanEntry(
    val start: String,
    val end: String,
    val showInformation: ShowInformation
)