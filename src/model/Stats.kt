package de.ironjan.kotlinplayground.model

data class Stats(
    val showInformation: ShowInformation,
    val genre: String,
    val maxNoOfWishesReached: Boolean,
    val maxNoOfGreetingsReached: Boolean,
    val track: Track
)

