package de.ironjan.kotlinplayground

import com.google.gson.Gson
import de.ironjan.kotlinplayground.model.PlanEntry
import de.ironjan.kotlinplayground.model.Stats
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.html.*
import kotlinx.html.*
import kotlinx.css.*
import io.github.rybalkinsd.kohttp.ext.httpGet
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import java.text.DateFormat

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)


val upstreamBaseUri = "https://mensaupb.herokuapp.com/metalonly/"
val statsUri = upstreamBaseUri + "stats"
val planUri = upstreamBaseUri + "plan"

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }

    routing {
        get("/") {
            call.respondHtml {
                planUri.httpGet().use {
                    val plan = Gson().fromJson(it.body()?.string(), Array<PlanEntry>::class.java)

                    body {
                        h1 { +"Plan" }

                        for (p in plan) {
                            div {
                                h2 { +p.showInformation.show }
                                p { +"${p.start} - ${p.end}" }
                                p { +p.showInformation.moderator }
                            }
                        }
                    }
                }
            }
        }

        get("/html-dsl") {
            call.respondHtml {
                body {
                    h1 { +"HTML" }
                    ul {
                        for (n in 1..10) {
                            li { +"$n" }
                        }
                    }
                }
            }
        }

        get("/api/plan") {
            // just forward upstream
            planUri.httpGet().use {
                val plan = Gson().fromJson(it.body()?.string(), Array<PlanEntry>::class.java)
                call.respond(Gson().toJson(plan))
            }
        }
        get("/api/stats") {
            // just forward upstream
            statsUri.httpGet().use {
                val stats = Gson().fromJson(it.body()?.string(), Stats::class.java)

                val message = Gson().toJson(stats)
                call.respond(message)
            }
        }

        get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = Color.red
                }
                p {
                    fontSize = 2.em
                }
                rule("p.myclass") {
                    color = Color.blue
                }
            }
        }
    }
}

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}
